package com.tinysquare;

import java.util.List;

public class Tab {

    public final String name;
    public final List<Log> logs;

    public Tab(String name, List<Log> logs) {
        this.name = name;
        this.logs = logs;
    }
}
