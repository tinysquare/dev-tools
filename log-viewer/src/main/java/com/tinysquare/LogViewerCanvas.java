package com.tinysquare;

import javafx.application.Platform;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LogViewerCanvas extends Canvas {

    double w = 800;
    double h = 600;

    private static final Paint BACKGROUND = new LinearGradient(
            0, 0, 1, 1, true, CycleMethod.NO_CYCLE,
            new Stop(1, Color.web("242424")),
            new Stop(0, Color.web("484848"))
    );

    private Color accentColor = Color.TEAL;
    private Color accentColorForeground = Color.WHITE;

    private Paint headerBackground = new LinearGradient(
            0, 0, 1, 1, true, CycleMethod.NO_CYCLE,
            new Stop(0, accentColor),
            new Stop(1, accentColor.interpolate(Color.BLACK, 0.2))
    );
    private static final Color TEXT_COLOR = Color.LIGHTGRAY;
    private Font textFont = Font.font("Lucida Console", 12);
    private final GraphicsContext gc;
    private List<Log> logs = new ArrayList<>();
    private List<Tab> tabs = new ArrayList<>();

    private int activeTab = 0;
    private List<Log> filteredLogs = new ArrayList<>();
    private int pageSize = 45;
    private int lineHeight = 20;
    private int offset = 0;

    private final Map<LogLevel, Boolean> levelToggles = new HashMap<>();

    private String highlightSearch = "fr.inconcept";
    private String filter = "";

    private String dateFormatString = "yyyy-MM-dd HH:mm:ss";
    private SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
    private boolean loading = false;

    private Date minDate;
    private Date maxDate;

    public LogViewerCanvas() {
        gc = super.getGraphicsContext2D();
        gc.setFont(textFont);
        for (LogLevel level : LogLevel.values()) {
            levelToggles.put(level, true);
        }
        levelToggles.put(LogLevel.DEBUG, false);
        gc.setFontSmoothingType(FontSmoothingType.LCD);
        setOnScroll(e -> {
            if (e.getDeltaY() > 0) {
                up();
            } else {
                down();
            }
        });
    }


    public void setW(double width) {
        setWidth(width);
        this.w = width;
        draw();
    }

    public void setH(double height) {
        setHeight(height);
        this.h = height;

        pageSize = (int) ((height - 110) / lineHeight);
        draw();
    }

    public void lineHeightPopup() {
        String height = getUserInput("Line height", "" + lineHeight);
        this.lineHeight = Integer.parseInt(height);
        pageSize = (int) ((h - 110) / lineHeight);
        draw();
    }

    public void setAccentColor(Color color) {
        accentColor = color;
        int red = (int) (255.0 * color.getRed());
        int green = (int) (255.0 * color.getGreen());
        int blue = (int) (255.0 * color.getBlue());
        if (red * 0.299 + green * 0.587 + blue * 0.114 > 150) {
            accentColorForeground = Color.BLACK;
        } else {
            accentColorForeground = Color.WHITE;
        }
        headerBackground = new LinearGradient(
                0, 0, 1, 1, true, CycleMethod.NO_CYCLE,
                new Stop(0, accentColor),
                new Stop(1, accentColor.interpolate(Color.BLACK, 0.2))
        );
        draw();
    }

    public Color getAccentColor() {
        return accentColor;
    }

    public void draw() {
        gc.setFill(BACKGROUND);
        gc.fillRect(0, 0, w, h);
        if (loading) {
            gc.setFill(Color.WHITE);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("Loading", w / 2, h / 2);
            return;
        }

        if (logs.isEmpty()) {
            gc.setFill(Color.WHITE);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("Open log file (" + KeyCombination.keyCombination("Shortcut+O").getDisplayText() + ")", w / 2, h / 2);
            return;
        }

        int levelBadgeWidth = 90;

        drawHeader(levelBadgeWidth);

        double dateWidth = textWidth(dateFormat.format(new Date()));
        int y = 95;
        int x = 105;
        Pattern pattern = Pattern.compile("(?i)(" + highlightSearch + ")");
        for (int i = offset; i < Math.min(offset + pageSize, filteredLogs.size() - 1); i++) {
            x = 105;
            Log log = filteredLogs.get(i);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFill(getLevelBackgroundColor(log.level));
            gc.fillRoundRect(5, y - 8, levelBadgeWidth, lineHeight - 4, 5, 5);
            gc.setFill(getLevelForegroundColor(log.level));
            gc.fillText(log.level.name(), 5 + levelBadgeWidth / 2.0, y);

            gc.setTextAlign(TextAlignment.LEFT);

            gc.setFill(TEXT_COLOR);

            if (dateFormatString.length() > 0 && log.date != null) {
                gc.fillText(dateFormat.format(log.date), x, y);
                x += dateWidth + 10;
            }
            Matcher matcher = pattern.matcher(log.message);
            boolean found = false;
            int maxPos = 0;
            int lastPos = 0;
            while (matcher.find()) {
                found = true;
                MatchResult matchResult = matcher.toMatchResult();
                maxPos = matchResult.end();
                String textBefore = log.message.substring(lastPos, matchResult.start());
                double textBeforeWidth = textWidth(textBefore);
                String matchedText = matchResult.group();
                double matchedTextWidth = textWidth(matchedText);
                gc.setFill(TEXT_COLOR);
                gc.fillText(textBefore, x, y);

                x += textBeforeWidth;
                gc.setFill(accentColor);
                gc.fillRoundRect(x - 2, y - 8, matchedTextWidth + 4, 16, 5, 5);
                gc.setFill(accentColorForeground);
                gc.fillText(matchResult.group(), x, y);
                x += matchedTextWidth;
                lastPos = maxPos;
            }
            if (!found) {
                gc.setFill(TEXT_COLOR);
                gc.fillText(log.message, x, y);
            } else if (maxPos < log.message.length()) {
                gc.setFill(TEXT_COLOR);
                gc.fillText(log.message.substring(maxPos), x, y);
            }
            y += lineHeight;
        }
    }

    private void drawHeader(int levelBadgeWidth) {
        gc.setFill(headerBackground);
        gc.fillRect(0, 0, w, 60);

        int x = 10;
        int y = 15;
        gc.setStroke(accentColorForeground);
        double levelPanelWidth = LogLevel.values().length * (levelBadgeWidth + 10);
        gc.strokeRoundRect(x - 5, y - 10, levelPanelWidth, 45, 5, 5);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFill(accentColorForeground);
        gc.fillText("LEVELS", x + levelPanelWidth / 2.0, y);
        y += 20;
        for (LogLevel level : LogLevel.values()) {
            boolean enabled = levelToggles.get(level) == Boolean.TRUE;
            if (enabled) {
                gc.setFill(getLevelBackgroundColor(level));
                gc.fillRoundRect(x, y - 8, levelBadgeWidth, 16, 5, 5);
                gc.setFill(getLevelForegroundColor(level));
            } else {
                gc.setStroke(getLevelBackgroundColor(level));
                gc.setFill(getLevelBackgroundColor(level));
                gc.strokeRoundRect(x, y - 8, levelBadgeWidth, 16, 5, 5);
            }
            gc.fillText("[" + level.name().substring(0, 1) + "]" + level.name().substring(1), x + levelBadgeWidth / 2.0, y);
            x += levelBadgeWidth + 10;
        }

        x += 10;
        y -= 20;
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setFill(accentColorForeground);
        if (highlightSearch.length() > 0) {
            double labelWidth = textWidth("Highlight: ");
            gc.fillText("Highlight: ", x, y);
            double highlightWidth = textWidth(highlightSearch);
            gc.setStroke(accentColorForeground);
            gc.setFill(accentColorForeground);
            gc.strokeRoundRect(x + labelWidth + 5, y - 10, highlightWidth + 10, 20, 5, 5);
            gc.fillText(highlightSearch, x + labelWidth + 10, y);
            y += 15;
        }
        gc.setFill(Color.WHITE);
        if (filter.length() > 0) {
            gc.fillText("Filter: " + filter, x, y);
            y += 15;
        }
        if (!logs.isEmpty()) {
            gc.setTextAlign(TextAlignment.RIGHT);
            int start = offset;
            int end = Math.min(offset + pageSize, filteredLogs.size());
            double ratio = end * 1.0 / filteredLogs.size();
            gc.fillText(start + "-" + end + "/" + filteredLogs.size(), w - 10, 50);

            gc.setFill(accentColor);
            gc.fillRect(0, 55, w * ratio, 5);
        }

        drawTabs(0, 61);
    }

    private void drawTabs(int x, int y) {
        gc.setTextAlign(TextAlignment.CENTER);
        for (int i = 0; i < tabs.size(); i++) {
            if (i == activeTab) {
                gc.setFill(accentColor);
            } else {
                gc.setFill(Color.GRAY);
            }
            double textWidth = textWidth(tabs.get(i).name);
            gc.fillRect(x, y, textWidth + 30, 20);
            gc.setFill(i == activeTab ? accentColorForeground : Color.WHITE);
            gc.fillText(tabs.get(i).name, x + textWidth / 2 + 15, y + 10);
            x += textWidth + 31;
        }
    }

    public Color getLevelBackgroundColor(LogLevel level) {
        switch (level) {
            case DEBUG:
                return Color.GRAY;
            case INFO:
                return Color.SKYBLUE;
            case WARN:
                return Color.GOLD;
            case ERROR:
                return Color.ORANGERED;
            default:
                return Color.LIGHTGRAY;
        }
    }

    public Color getLevelForegroundColor(LogLevel level) {
        switch (level) {
            case DEBUG:
            case ERROR:
                return Color.WHITE;
            default:
                return Color.BLACK;
        }
    }

    private String getUserInput(String label, String defaultValue) {
        TextInputDialog td = new TextInputDialog(defaultValue);
        td.setHeaderText(null);
        td.setGraphic(null);
        td.setTitle(label);
        Optional<String> input = td.showAndWait();
        return input.orElse("");
    }

    public void fontPopup() {
        ChoiceDialog<String> fontChoiceDialog = new ChoiceDialog<>(textFont.getName(), Font.getFontNames());
        fontChoiceDialog.setHeaderText(null);
        fontChoiceDialog.setTitle("Choose font");
        fontChoiceDialog.setGraphic(null);
        Optional<String> choosenFont = fontChoiceDialog.showAndWait();
        if (choosenFont.isPresent()) {
            String size = getUserInput("Font size", (int) textFont.getSize() + "");
            int fontSize = Integer.parseInt(size);
            textFont = Font.font(choosenFont.get(), fontSize);
            gc.setFont(textFont);
        }
        draw();
    }

    public void dateFormatPopup() {
        dateFormatString = getUserInput("Date format", dateFormatString);
        if (dateFormatString.length() > 0) {
            dateFormat = new SimpleDateFormat(dateFormatString);
        }
        draw();
    }

    public void keyPressed(KeyEvent e) {
        if (e.isControlDown()) {
            switch (e.getCode()) {
                case NUMPAD1:
                case NUMPAD2:
                case NUMPAD3:
                case NUMPAD4:
                case NUMPAD5:
                case NUMPAD6:
                case NUMPAD7:
                case NUMPAD8:
                case NUMPAD9:
                    selectTab(Integer.parseInt(e.getText()));
                    break;
                default:
                    break;
            }
        }
        switch (e.getCode()) {
            case PAGE_DOWN:
                down();
                break;
            case PAGE_UP:
                up();
                break;
        }
    }

    public void filter() {
        filter = getUserInput("Filter", filter).toLowerCase();
        filterLogs();
    }

    public void highlight() {
        highlightSearch = getUserInput("Highlight text", highlightSearch);
        draw();
    }

    public void dateFilter() {
        try {
            String minDateInput = getUserInput("Min date", minDate != null ? dateFormat.format(minDate) : "");
            minDate = minDateInput.length() > 0 ? dateFormat.parse(minDateInput) : null;
            if (minDate != null && maxDate != null && minDate.getTime() > maxDate.getTime()) {
                maxDate = new Date(minDate.getTime() + 3_600 * 1_000);
            }
            String maxDateInput = getUserInput("Max date", maxDate != null ? dateFormat.format(maxDate) : "");
            maxDate = maxDateInput.length() > 0 ? dateFormat.parse(maxDateInput) : null;
            filterLogs();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private void down() {
        offset += pageSize;
        if (offset > filteredLogs.size()) {
            offset = filteredLogs.size() - 2;
        }
        draw();
    }

    private void up() {
        offset -= pageSize;
        if (offset < 0) {
            offset = 0;
        }
        draw();
    }

    public void start() {
        offset = 0;
        draw();
    }

    public void end() {
        offset = filteredLogs.size() - pageSize;
        draw();
    }

    public void selectTab(int index) {
        if (index >= 1 && index <= tabs.size()) {
            activeTab = index - 1;
            if (activeTab < tabs.size() && activeTab >= 0) {
                setLogs(tabs.get(activeTab).logs);
                filterLogs();
            }
        }
    }

    public void closeActiveTab() {
        tabs.remove(activeTab);
        activeTab++;
        if (activeTab >= tabs.size() - 1) {
            activeTab = tabs.size() - 2;
        }
        if (!tabs.isEmpty()) {
            setLogs(tabs.get(activeTab).logs);
            filterLogs();
        } else {
            setLogs(Collections.emptyList());
            filterLogs();
        }
    }

    public void toggleLogLevel(LogLevel level) {
        Boolean current = levelToggles.get(level);
        levelToggles.put(level, !current);
        filterLogs();
    }

    public void openFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setTitle("Open log file");
        File choosenFile = fileChooser.showOpenDialog(null);

        if (choosenFile != null) {

            loading = true;
            draw();
            new Thread(() -> {
                setLogs(LogParser.parseLogs(choosenFile));
                tabs.add(new Tab(choosenFile.getName(), logs));
                activeTab = tabs.size() - 1;
                loading = false;
                Platform.runLater(this::filterLogs);
            }).start();
        }
    }

    private void setLogs(List<Log> logs) {
        this.logs = logs;
        minDate = logs.stream().filter(log -> log.date != null).findFirst().map(log -> log.date).orElse(null);
        maxDate = null;
        for (int i = logs.size() - 1; i > 0; i--) {
            if (logs.get(i).date != null) {
                maxDate = logs.get(i).date;
                break;
            }
        }
    }

    private double textWidth(String text) {
        Text label = new Text(text);
        label.setFont(textFont);
        return label.getLayoutBounds().getWidth();
    }

    private void filterLogs() {
        offset = 0;
        if (minDate != null || maxDate != null) {
            filteredLogs.clear();
            boolean ok = false;
            Pattern regex = Pattern.compile(".*" + filter + ".*");
            for (Log log : logs) {
                if (ok) {
                    if (levelToggles.get(log.level) == Boolean.TRUE && regex.matcher(log.message.toLowerCase()).matches()) {
                        filteredLogs.add(log);
                    }
                    if (maxDate != null && log.date != null && log.date.getTime() > maxDate.getTime()) {
                        break;
                    }
                    continue;
                }
                if (minDate != null && log.date != null && log.date.getTime() >= minDate.getTime()) {
                    ok = true;
                    if (levelToggles.get(log.level) == Boolean.TRUE && regex.matcher(log.message.toLowerCase()).matches()) {
                        filteredLogs.add(log);
                    }
                }
            }
        } else {
            filteredLogs = logs.stream().filter(
                    log -> levelToggles.get(log.level) == Boolean.TRUE && log.message.toLowerCase().contains(filter)
            ).collect(Collectors.toList());
        }
        draw();
    }
}
