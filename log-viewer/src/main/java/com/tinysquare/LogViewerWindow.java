package com.tinysquare;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.awt.*;

public class LogViewerWindow extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.setMaximized(true);
        LogViewerCanvas logViewerCanvas = new LogViewerCanvas();

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(logViewerCanvas);
        MenuBar menuBar = createMenu(logViewerCanvas);
        borderPane.setTop(menuBar);
        Scene scene = new Scene(borderPane);
        scene.widthProperty().addListener((observable, oldValue, newValue) ->
                logViewerCanvas.setW(newValue.doubleValue())
        );
        scene.heightProperty().addListener((observable, oldValue, newValue) ->
                logViewerCanvas.setH(newValue.doubleValue())
        );
        scene.setOnKeyPressed(e -> logViewerCanvas.keyPressed(e));

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private MenuBar createMenu(LogViewerCanvas logViewerCanvas) {
        MenuBar menuBar = new MenuBar();
        Menu file = new Menu("File");

        MenuItem fileOpen = new MenuItem("Open log file");
        fileOpen.setAccelerator(KeyCombination.keyCombination("Shortcut+O"));
        fileOpen.setOnAction(e -> logViewerCanvas.openFile());

        MenuItem closeTab = new MenuItem("Close active tab");
        closeTab.setAccelerator(KeyCombination.keyCombination("Shortcut+W"));
        closeTab.setOnAction(e -> logViewerCanvas.closeActiveTab());

        file.getItems().addAll(fileOpen);


        Menu navigate = new Menu("Navigate");

        MenuItem filter = new MenuItem("Filter");
        filter.setOnAction(e -> logViewerCanvas.filter());
        filter.setAccelerator(KeyCombination.keyCombination("Shortcut+F"));

        MenuItem highlight = new MenuItem("Highlight");
        highlight.setOnAction(e -> logViewerCanvas.highlight());
        highlight.setAccelerator(KeyCombination.keyCombination("Shortcut+H"));

        MenuItem goToStart = new MenuItem("Go to start of file");
        goToStart.setAccelerator(KeyCombination.keyCombination("Shortcut+Home"));
        goToStart.setOnAction(e -> logViewerCanvas.start());

        MenuItem goToEnd = new MenuItem("Go to end of file");
        goToEnd.setAccelerator(KeyCombination.keyCombination("Shortcut+End"));
        goToEnd.setOnAction(e -> logViewerCanvas.end());

        MenuItem dateRange = new MenuItem("Date filter");
        dateRange.setAccelerator(KeyCombination.keyCombination("Shortcut+D"));
        dateRange.setOnAction(e -> logViewerCanvas.dateFilter());

        navigate.getItems().addAll(closeTab, filter, highlight, goToStart, goToEnd, dateRange);
        MenuItem font = new MenuItem("Set font");
        font.setOnAction(e -> logViewerCanvas.fontPopup());

        MenuItem dateFormat = new MenuItem("Date format");
        dateFormat.setOnAction(e -> logViewerCanvas.dateFormatPopup());

        MenuItem lineHeight = new MenuItem("Line height");
        lineHeight.setOnAction(e -> logViewerCanvas.lineHeightPopup());

        MenuItem colorAccent = new MenuItem("Color accent");
        ColorPicker colorPicker = new ColorPicker(logViewerCanvas.getAccentColor());
        colorPicker.getCustomColors().addAll(Color.TEAL, Color.FIREBRICK, Color.LIGHTSLATEGRAY,
                Color.PALETURQUOISE,
                Color.PALEGOLDENROD,
                Color.PALEGREEN,
                Color.PALEVIOLETRED
        );
        colorPicker.setOnAction(e -> {
            logViewerCanvas.setAccentColor(colorPicker.getValue());
        });
        colorAccent.setGraphic(colorPicker);

        Menu options = new Menu("Options");
        options.getItems().addAll(
                font,
                dateFormat,
                lineHeight,
                colorAccent
        );
        Menu levels = new Menu("Levels");
        for (LogLevel level : LogLevel.values()) {
            CheckMenuItem levelToggle = new CheckMenuItem("Level: " + level.name());
            levelToggle.setSelected(level != LogLevel.DEBUG);
            levelToggle.setAccelerator(KeyCombination.keyCombination(level.name().substring(0, 1)));
            levelToggle.setOnAction(e -> logViewerCanvas.toggleLogLevel(level));
            levels.getItems().add(levelToggle);
        }

        menuBar.getMenus().addAll(
                file,
                navigate,
                levels,
                options
        );
        return menuBar;
    }
}
