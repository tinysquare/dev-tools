package com.tinysquare;

import javafx.application.Application;

import java.io.IOException;

public class LogParserMain {

    public static void main(String[] args) throws IOException {
        Application.launch(LogViewerWindow.class, args);
        System.exit(0);
    }
}
