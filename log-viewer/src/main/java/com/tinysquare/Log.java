package com.tinysquare;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Log {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public final String message;
    public final LogLevel level;
    public final Date date;

    public Log(String message) {
        this.message = message;
        level = LogLevel.NONE;
        date = null;
    }

    public Log(String message, String level, String date) {
        this.message = message;
        this.level = LogLevel.from(level);
        try {
            this.date = DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
