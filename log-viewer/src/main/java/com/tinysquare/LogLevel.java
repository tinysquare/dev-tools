package com.tinysquare;

public enum LogLevel {
    NONE,
    DEBUG,
    INFO,
    WARN,
    ERROR;

    public static LogLevel from(String level) {
        if (level == null) {
            return NONE;
        }
        switch (level.toUpperCase()) {
            case "DEBUG":
                return DEBUG;
            case "INFO":
                return INFO;
            case "WARN":
                return WARN;
            case "ERROR":
                return ERROR;
            default:
                return NONE;
        }
    }
}
