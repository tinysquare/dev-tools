package com.tinysquare;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogParser {

    private static final Pattern LOG_PATTERN = Pattern.compile("(?<time>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{3}) +" +
            "(?<level>DEBUG|INFO|WARN|ERROR)?"
            + ".*\\[(?<thread>.*)\\]"
            + " .*\\.?(?<class>[^:]+)"
            + " *: (?<message>.*)"
    );

    public static List<Log> parseLogs(File file) {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Log> logs = new ArrayList<>();
        for (String line : lines) {
            Matcher matcher = LOG_PATTERN.matcher(line);
            if (matcher.find()) {
                logs.add(new Log(matcher.group("message"), matcher.group("level"), matcher.group("time")));
            } else {
                logs.add(new Log(line));
            }
        }
        return logs;
    }
}
